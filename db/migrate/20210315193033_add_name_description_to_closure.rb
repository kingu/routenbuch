class AddNameDescriptionToClosure < ActiveRecord::Migration[6.1]
  def change
    rename_column :closures, :description, :name
    add_column :closures, :description, :text, default: '', null: false

    rename_column :closure_templates, :description, :closure_name
  end
end
