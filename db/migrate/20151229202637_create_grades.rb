class CreateGrades < ActiveRecord::Migration
  def change
    create_table :grades do |t|
      t.string :scale
      t.string :scope
      t.string :grade
      t.integer :difficulty

      t.timestamps null: false
    end
  end
end
