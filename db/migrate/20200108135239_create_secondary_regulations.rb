class CreateSecondaryRegulations < ActiveRecord::Migration[6.0]
  def change
    create_table :secondary_regulations do |t|
      t.string :description, null: false, default: ''
      t.references :geo_ref, null: false, foreign_key: true
      t.references :regulation, null: false, foreign_key: true

      t.timestamps
    end
  end
end
