class ExtendFirstAscentPerson < ActiveRecord::Migration[6.0]
  def up
    add_column :first_ascent_people, :first_name, :string, null: false, default: ''
    add_column :first_ascent_people, :last_name, :string, null: false, default: ''
    add_column :first_ascent_people, :dead, :boolean, null: false, default: false
    FirstAscentPerson.all.each do |p|
      (first, last) = p.attributes['name'].split(/\s+/, 2)
      # if only a single value is present use it as last_name
      if last.blank? && first.present?
        last = first
        first = ''
      end
      p.update!(
        first_name: first || '',
        last_name: last || '-'
      )
    end

    remove_column :first_ascent_people, :name
  end

  def down
    add_column :first_ascent_people, :name, :string, null: false, default: ''

    FirstAscentPerson.all.each do |p|
      p.update!(
        name: "#{p.first_name} #{p.last_name}",
      )
    end

    remove_column :first_ascent_people, :first_name
    remove_column :first_ascent_people, :last_name
    remove_column :first_ascent_people, :dead
  end
end
