class AddAttemptsToAscent < ActiveRecord::Migration[5.0]
  def change
    add_column :ascents, :attempts, :integer
  end
end
