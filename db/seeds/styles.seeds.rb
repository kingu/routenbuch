
styles = [
  [N_("Alpine free"), true, "alternate_lead", "free", 50],
  [N_("Redpoint"), true, "lead", "free", 90],
  [N_("Redpoint attempt"), false, "lead", "tainted", 80],
  [N_("Flash"), true, "lead", "free", 60],
  [N_("Onsight"), true, "lead", "free", 60],
  [N_("Top rope"), false, "toprope", "free", 70],
  [N_("Top Rope with ascent clamp"), false, "toprope", "free", 40],
  [N_("Top Rope attempt with ascent clamp"), false, "toprope", "tainted", 40]
]
styles.each do |name, redpoint, belay_style, sportive_style, priority|
  begin
    Style.create!(
      name: name,
      redpoint: redpoint,
      belay_style: belay_style,
      sportive_style: sportive_style,
      priority: priority
    )
  rescue ActiveRecord::RecordInvalid => e
    raise e unless e.message =~ /Name has already been taken/
  end
end
