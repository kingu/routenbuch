require 'csv'

after :vendors do
  CSV.foreach(
    Rails.root.join('db', 'seeds', 'products.csv'),
    headers: true,
    encoding: 'utf-8'
  ) do |row|
    vendor = Vendor.find_by!(name: row['vendor']) unless row['vendor'].blank?
    p = Product.find_or_create_by!(
      name: row['name'],
      vendor: vendor
    ) do |p|
      p.article_number = row['article_number']
      p.kind = row['kind']
      p.standard = row['standard']
      p.url = row['url']
    end
    p.update!(
      article_number: row['article_number'],
      kind: row['kind'],
      standard: row['standard'],
      url: row['url']
    )
  end
end
