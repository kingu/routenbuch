namespace :gettext do
  def files_to_translate
    Dir.glob("{app,lib,config,locale}/**/*.rb") \
      + Dir.glob("app/views/**/*.{erb,haml,slim,builder}") \
      + Dir.glob("db/seeds/*.rb")
  end
end
