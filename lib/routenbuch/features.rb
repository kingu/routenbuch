module Routenbuch
  class Features
    def initialize(config)
      config ||= {}
      @config = {
        'ascents' => true,
        'ticklists' => true
      }.merge(config)
    end

    def feature?(feature)
      return @config[feature] if @config.key? feature

      return false
    end

    def method_missing(m, *args, &block)
      if (match = /^(.*)\?$/.match(m))
        feature?(match[1])
      else
        raise ArgumentError.new("Method `#{m}` doesn't exist.")
      end
    end
  end
end
