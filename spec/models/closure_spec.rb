require 'rails_helper'

RSpec.describe Closure, type: :model do
  context 'closure' do
    let(:attributes) do
      {
        regular_start_month: 2,
        regular_start_day_of_month: 1,
        regular_end_month: 6,
        regular_end_day_of_month: 30,
      }
    end
    let(:closure) { create :closure, **attributes }
    let(:year) { 2021 }
    let(:time) { Date.new(year, 1, 1) }
    let(:calc) { closure.calc_at(time) }

    it 'calculations' do
      expect(calc.start_year).to eq year
      expect(calc.end_year).to eq year
      expect(calc.after_end?).to be false
      expect(calc.multi_year?).to be false
      expect(calc.start_at).to eq Date.new(year, 2, 1)
      expect(calc.regular_start_at).to eq Date.new(year, 2, 1)
      expect(calc.end_at).to eq Date.new(year, 6, 30)
      expect(calc.open_at).to eq Date.new(year, 7, 1)
      expect(calc.regular_end_at).to eq Date.new(year, 6, 30)
      expect(calc.active?).to be false
      expect(calc.complete?).to be true
    end

    it 'validation' do
      expect { closure.validate! }.not_to raise_error
    end

    context 'on first closed day' do
      let(:time) { Date.new(year, 2, 1) }

      it 'calculations' do
        expect(calc.after_end?).to be false
        expect(calc.active?).to be true
      end
    end

    context 'on last closed day' do
      let(:time) { Date.new(year, 6, 30) }

      it 'calculations' do
        expect(calc.after_end?).to be false
        expect(calc.active?).to be true
      end
    end

    context 'on first opened day' do
      let(:time) { Date.new(year, 7, 1) }

      it 'calculations' do
        expect(calc.after_end?).to be true
        expect(calc.active?).to be false
      end
    end

    context 'with season_closure' do
      before do
        @season_closure = closure.season_closures.create!(
          description: 'no breadings',
          end_month: 5,
          end_day_of_month: 10,
          year: year
        )
        closure.reload
      end

      it 'new end date' do
        expect(closure.end_at).to eq Date.new(year, 5, 10)
      end

      it 'end after update' do
        @season_closure.update end_day_of_month: 31
        expect(closure.end_at).to eq Date.new(year, 5, 31)
      end

      it 'end after removal' do
        @season_closure.destroy!
        expect(closure.end_at).to eq Date.new(year, 6, 30)
      end
    end
  end
end
