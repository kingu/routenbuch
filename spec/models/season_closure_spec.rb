require 'rails_helper'

RSpec.describe SeasonClosure, type: :model do
  let(:attributes) { {} }
  let(:season_closure) { create :season_closure, **attributes }

  context 'season_closure' do
    it 'validation' do
      expect { season_closure.validate! }.not_to raise_error
    end
  end
end
