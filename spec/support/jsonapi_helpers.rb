module JsonapiHelpers
  def jsonapi_object(attributes = {})
    {
      properties: {
        id: { type: :string },
        type: { type: :string },
        attributes: attributes,
        meta: {
          type: :object,
          properties: {
            cache_version: { type: :string }
          }
        },
        relationships: { type: :object }
      },
      required: [ 'id', 'type', 'attributes' ]
    }
  end

  def jsonapi_collection(items = {})
    {
      type: :object,
      properties: {
        data: {
          type: :array,
          items: items
        },
        meta: { '$ref': '#/components/schemas/jsonapi_collection_meta' },
        links: { '$ref': '#/components/schemas/jsonapi_collection_links' }
      },
      required: [ 'data' ]
    }
  end
end
