require 'rails_helper'

RSpec.describe Admin::UsersController, type: :request do
  login_user :admin_user

  before(:each) do
    @member = create(:member_user)
    @admin = create(:admin_user)
    @contributor = create(:contributor_user)
  end

  it 'index' do
    get admin_users_path
    expect(response).to have_http_status(:ok)
  end

  it 'members index' do
    get members_admin_users_path
    expect(response).to have_http_status(:ok)
  end

  it 'contributors index' do
    get contributors_admin_users_path
    expect(response).to have_http_status(:ok)
  end

  it 'admins index' do
    get admins_admin_users_path
    expect(response).to have_http_status(:ok)
  end

  it 'autocomplete' do
    get autocomplete_admin_users_path, params: { term: @member.name }
    expect(response).to have_http_status(:ok)
    expect(response.content_type).to eq('application/json; charset=utf-8')
    json_response = JSON.parse(response.body)
    expect(json_response.size).to eq 1
    expect(json_response[0].keys).to match_array %w[id value label]
  end

  it 'show' do
    get admin_user_path(@member)
    expect(response).to have_http_status(:ok)
  end

  it 'new' do
    get new_admin_user_path
    expect(response).to have_http_status(:ok)
  end

  it 'edit' do
    get edit_admin_user_path(@member)
    expect(response).to have_http_status(:ok)
  end

  it 'create' do
    post(
      admin_users_path,
      params: {
        user: {
          name: 'Karl Ranseier',
          email: 'karl@ranseier.de',
          role: 'member',
          password: '345P4s5W0rD!',
          password_confirmation: '345P4s5W0rD!'
        }
      }
    )
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include('User was successfully created.')
  end

  it 'create with validation error' do
    post(
      admin_users_path,
      params: {
        user: {
          email: 'invalid',
        }
      }
    )
    expect(response).to have_http_status(:ok)
    expect(response.body).to include('Please review the problems below')
  end

  it 'update' do
    patch(
      admin_user_path(@member),
      params: {
        user: {
          name: 'update me!'
        }
      }
    )
    expect(response).to have_http_status(:found)
    @member.reload
    expect(@member.name).to eq('update me!')
    follow_redirect!
    expect(response.body).to include('User was successfully updated.')
  end

  it 'update with validation error' do
    patch(
      admin_user_path(@member),
      params: {
        user: {
          email: 'invalid'
        }
      }
    )
    expect(response).to have_http_status(:ok)
    expect(response.body).to include('Please review the problems below')
  end

  it 'destroy' do
    delete admin_user_path(@member)
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include('User was successfully removed.')
    expect(User.exists? @member.id).to be false
  end
end
