require 'rails_helper'

RSpec.describe VersionsController, type: :request do
  login_user :admin_user

  before(:each) do
    @region = create(:region)
    @closure = create(:closure, geo_ref: @region)
    @season_closure = create(:season_closure, closure: @closure)
    @regulation = create(:regulation, geo_ref: @region)
    @crag = create(:crag, parent: @region)
    @route = create(:route, geo_ref: @crag)
    @inventory = create(:inventory, route: @route)
    @route_link = create(:route_link, route_from: @route)
    PaperTrail.request.controller_info = { user_id: @user.id }
    @route.update(description: 'updated')
    @closure.update(description: 'updated')
    @season_closure.update(description: 'updated')
    @inventory.update(description: 'updated')
    @route_link.update(first_anchor_number: 2)
  end

  it 'global index' do
    get versions_path
    expect(response).to have_http_status(:ok)
  end

  it 'region index' do
    get geo_ref_versions_path(@region)
    expect(response).to have_http_status(:ok)
  end

  it 'crag index' do
    get geo_ref_versions_path(@crag)
    expect(response).to have_http_status(:ok)
  end

  it 'route index' do
    get route_versions_path(@route)
    expect(response).to have_http_status(:ok)
  end

  it 'user index' do
    get user_versions_path(@user)
    expect(response).to have_http_status(:ok)
  end
end
