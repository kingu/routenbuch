require 'rails_helper'

RSpec.describe InventoriesController, type: :request do
  login_user :admin_user

  before(:each) do
    @route = create(:route)
    @inventory = create(:inventory)
  end

  it 'index on route' do
    get route_inventories_path(@route)
    expect(response).to have_http_status(:ok)
  end

  it 'new' do
    get new_route_inventory_path(@route)
    expect(response).to have_http_status(:ok)
  end

  it 'edit' do
    get edit_route_inventory_path(@route, @inventory)
    expect(response).to have_http_status(:ok)
  end
end
