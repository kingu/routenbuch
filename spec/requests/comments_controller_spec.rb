require 'rails_helper'

RSpec.describe CommentsController, type: :request do
  login_user :admin_user

  before(:each) do
    @route_comment = create(:route_comment, user: @user)
    @geo_ref_comment = create(:geo_ref_comment, user: @user)
  end

  it 'index' do
    get comments_path
    expect(response).to have_http_status(:ok)
  end

  it 'index on geo_ref' do
    get geo_ref_comments_path(@geo_ref_comment.target)
    expect(response).to have_http_status(:ok)
  end

  it 'index on route' do
    get route_comments_path(@route_comment.target)
    expect(response).to have_http_status(:ok)
  end

  it 'new geo_ref comment' do
    get new_geo_ref_comment_path(@geo_ref_comment.target)
    expect(response).to have_http_status(:ok)
  end

  it 'new route comment' do
    get new_route_comment_path(@route_comment.target)
    expect(response).to have_http_status(:ok)
  end

  it 'create geo_ref comment' do
    post(
      geo_ref_comments_path(@geo_ref_comment.target),
      params: {
        comment: {
          body: 'test comment'
        }
      }
    )
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include("Comment was successfully created.")
  end

  it 'create route comment' do
    post(
      route_comments_path(@route_comment.target),
      params: {
        comment: {
          body: 'test comment'
        }
      }
    )
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include("Comment was successfully created.")
  end

  it 'edit' do
    get edit_comment_path(@route_comment)
    expect(response).to have_http_status(:ok)
  end

  it 'update' do
    patch(
      comment_path(@route_comment),
      params: {
        comment: {
          body: 'update me!'
        }
      }
    )
    expect(response).to have_http_status(:found)
    @route_comment.reload
    expect(@route_comment.body).to eq 'update me!'
    follow_redirect!
    expect(response.body).to include("Comment was successfully updated.")
  end

  it 'destroy' do
    delete comment_path(@route_comment)
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include("Comment was successfully removed.")
    expect(Comment.exists? @route_comment.id).to be false
  end
end
