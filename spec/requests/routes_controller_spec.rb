require 'rails_helper'

RSpec.describe RoutesController, type: :request do
  login_user :admin_user

  before(:each) do
    @route = create(:route)
  end

  it 'autocomplete' do
    get autocomplete_routes_path, params: { term: @route.name }
    expect(response).to have_http_status(:ok)
    expect(response.content_type).to eq('application/json; charset=utf-8')
    json_response = JSON.parse(response.body)
    expect(json_response.size).to eq 1
    expect(json_response[0].keys).to match_array %w[id value label]
  end

  it 'index' do
    get routes_path
    expect(response).to have_http_status(:ok)
  end

  it 'index on geo_ref' do
    get geo_ref_routes_path(@route.geo_ref)
    expect(response).to have_http_status(:ok)
  end

  it 'new' do
    get new_geo_ref_route_path(create(:crag))
    expect(response).to have_http_status(:ok)
  end

  it 'create' do
    post(
      geo_ref_routes_path(create(:crag)),
      params: {
        route: {
          name: 'Kokolores'
        }
      }
    )
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include("Route was successfully created.")
  end

  it 'show' do
    get route_path(@route)
    expect(response).to have_http_status(:ok)
  end

  it 'edit' do
    get edit_route_path(@route)
    expect(response).to have_http_status(:ok)
  end

  it 'update' do
    patch(
      route_path(@route),
      params: {
        route: {
          description: 'update me!'
        }
      }
    )
    expect(response).to have_http_status(:found)
    @route.reload
    expect(@route.description).to eq('update me!')
    follow_redirect!
    expect(response.body).to include("Route was successfully updated.")
  end

  it 'destroy' do
    delete route_path(@route)
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include("Route was successfully destroyed.")
    expect(Route.exists? @route.id).to be false
  end
end
