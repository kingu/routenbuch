FactoryBot.define do
  factory :user, aliases: [:member_user] do
    name { 'John Doe' }
    sequence :email do |n|
      "person#{n}@example.com"
    end
    password { 'g00dp4$Sw0rT!' }
    role { 'member' }

    factory :admin_user, parent: :user do
      name { 'Big boss' }
      role { 'admin' }
      scope_embed_api { true }
    end

    factory :contributor_user, parent: :user do
      name { 'Hausmeister Krause' }
      role { 'contributor' }
    end

    factory :guest_user, parent: :user do
      role { 'guest' }
    end
  end
end
