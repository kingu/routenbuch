FactoryBot.define do
  factory :route_link do
    route_from factory: :route
    route_to { association :route, geo_ref: route_from.geo_ref }
    first_anchor_number { 3 }
    last_anchor_number { 7 }
    kind { 'overlap' }
  end
end
