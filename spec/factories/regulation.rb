FactoryBot.define do
  factory :regulation do
    geo_ref factory: :region
    name { 'Kletterkonzept Gößweinstein' }
    abbr { 'KZG' }
    body { lorem_ipsum }

    after(:create) do |object, evaluator|
      create_list(:regulated_geo_ref, 3, regulation: object)
    end
  end
end
