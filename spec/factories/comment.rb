FactoryBot.define do
  factory :comment, aliases: [:geo_ref_comment] do
    body { '<p>Hello World!</p>' }
    target factory: :crag
    user

    factory :route_comment, parent: :comment do
      target factory: :route
    end
  end
end
