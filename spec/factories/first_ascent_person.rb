FactoryBot.define do
  factory :first_ascent_person do
    first_name { 'Kurt' }
    last_name { 'Albert' }
    alternative_names { ['Der Kurt'] }
  end
end
