Sidekiq.configure_client do |config|
  config.redis = Routenbuch.config[:redis] || {}
end

Sidekiq.configure_server do |config|
  config.redis = Routenbuch.config[:redis] || {}
end

schedule_file = Rails.root.join('config/schedule.yml')

if File.exist?(schedule_file) && Sidekiq.server?
  Sidekiq::Cron::Job.load_from_hash YAML.load_file(schedule_file)
end
