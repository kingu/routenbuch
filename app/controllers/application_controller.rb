class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception, prepend: true

  before_action :authenticate_user! unless Routenbuch.public?
  check_authorization :unless => :devise_controller?
  # hack to internal @_authorized used by cancancan
  def skip_authorization_check(*args)
    @_authorized = true
  end

  include BackNavigatable

  before_action :set_locale
  def set_locale
    if current_user&.locale.present?
      new_locale = FastGettext.set_locale(current_user.locale)
      I18n.locale = new_locale
    else
      set_gettext_locale
    end

    FastGettext.reload! if Rails.env.development?
  end

  before_action :check_access_scope
  def check_access_scope
    # login, logout, etc.
    return if devise_controller?
    # error pages, etc.
    return if access_scope == :public
    # guest users
    return if current_user.nil?

    authorize! :scope, access_scope
  end

  def access_scope
    :web_ui
  end

  # set user on versions
  before_action :set_paper_trail_whodunnit
  def info_for_paper_trail
    user_id = if current_user.is_a? User
                current_user.id
              end
    {
      user_id: user_id
    }
  end

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden, content_type: 'text/html' }
      format.html { redirect_to forbidden_path, notice: exception.message }
      format.js   { head :forbidden, content_type: 'text/html' }
    end
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  helper_method :navigation_context, :show_global_search, :show_navigation, :page_title, :logo_url, :custom_css_url

  private

  def page_title
    Routenbuch.instance_name
  end

  def navigation_context
    'default'
  end

  def show_global_search
    show_navigation
  end

  def show_navigation
    return false if !Routenbuch.public? && current_user.nil?

    true
  end

  def logo_url
    Routenbuch.config[:logo_url]
  end

  def custom_css_url
    Routenbuch.config[:custom_css_url]
  end
end
