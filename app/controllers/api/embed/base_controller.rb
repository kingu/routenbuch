module Api
  module Embed
    class BaseController < ApiController
      def access_scope
        :embed_api
      end
    end
  end
end
