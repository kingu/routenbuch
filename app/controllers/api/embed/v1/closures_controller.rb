module Api
  module Embed
    module V1
      class ClosuresController < Api::Embed::BaseController
        skip_authorization_check only: %i[index feed]
        before_action :set_base

        def index
          @closures = @base_relation \
            .accessible_by(current_ability, :read) \
            .includes(:geo_ref, :relevant_season_closures) \
            .page(params[:page])

          eager_load_associated_items
          render json: serialized_resultset
        end

        def feed
          @closures = @base_relation \
            .accessible_by(current_ability, :read) \
            .includes(:geo_ref, :relevant_season_closures) \
            .where.not(active_changed_at: nil) \
            .reorder(active_changed_at: :desc)
            .page(params[:page])

          eager_load_associated_items
          render json: serialized_resultset
        end

        private

        def set_base
          @base_relation = \
            if params[:geo_ref_id]
              @base_object = GeoRef.find(params[:geo_ref_id])
              authorize! :read, @base_object
              @base_object.self_and_descendent_closures
            else
              Closure.order('geo_refs.name')
            end
        end

        def eager_load_associated_items
          @geo_refs = GeoRef \
            .joins(:applicable_closures).where('closures.id': @closures) \
            .includes(:route_first, :route_last) \
            .accessible_by(current_ability, :read) \
            .select('closures.id AS closure_id, geo_refs.*') \
            .order(:name) \
            .group_by(&:closure_id)
          @routes = Route \
            .joins(:closures, :geo_ref).where('closures.id': @closures) \
            .accessible_by(current_ability, :read) \
            .select('closures.id AS closure_id, routes.*') \
            .order('geo_refs.name', :priority) \
            .group_by(&:closure_id)
        end

        def serialized_resultset
          ClosureSerializer.new(
            @closures,
            links: jsonapi_paginate_links(@closures),
            meta: jsonapi_paginate_meta(@closures),
            params: {
              routes: @routes,
              geo_refs: @geo_refs
            }
          ).serializable_hash.to_json
        end
      end
    end
  end
end
