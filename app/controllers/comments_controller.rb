class CommentsController < ApplicationController
  before_action :set_comment, only: [:edit, :update, :destroy]
  before_action :set_base, only: [:index, :new, :create]
  skip_authorization_check :only => [:index]

  def index
    @q = @base_relation.ransack(params[:q])
    @comments = @q.result \
      .includes(:user) \
      .accessible_by(current_ability, :read) \
      .order(**base_order) \
      .page(params[:page])
  end

  def new
    @comment = Comment.new
    @comment.user = current_user
    @comment.target = @base_object
    authorize! :create, @comment
  end

  def edit
    authorize! :update, @comment
  end

  def create
    @comment = Comment.new(comment_params)
    @comment.user = current_user
    @comment.target = @base_object
    authorize! :create, @comment

    if @comment.save
      redirect_to polymorphic_path([@base_object.as_base_class, Comment]),
        notice: _('Comment was successfully created.')
    else
      render :new
    end
  end

  def update
    authorize! :update, @comment
    if @comment.update(comment_params)
      redirect_to polymorphic_path([@comment.target.as_base_class, Comment]),
        notice: _('Comment was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @comment
    @comment.destroy
    redirect_to polymorphic_path([@comment.target.as_base_class, Comment]),
      notice: _('Comment was successfully removed.')
  end

  private

  def set_base
    @base_object, @base_relation = \
      if params[:route_id]
        route = Route.find(params[:route_id])
        [ route, route.comments ]
      elsif params[:geo_ref_id]
        geo_ref = GeoRef.find(params[:geo_ref_id])
        [ geo_ref, geo_ref.comments ]
      else
        [ nil, Comment ]
      end
    authorize! :read, @base_object unless @base_object.nil?
  end
  
  def base_order
    case @base_object
    when Route, GeoRef
      { pinned: :desc, created_at: :desc }
    else
      { created_at: :desc }
    end
  end

  def set_comment
    @comment = Comment.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(
      :body,
      :kind,
      :private,
      :pinned,
    )
  end
end
