class SeasonClosure < ApplicationRecord
  belongs_to :closure

  validates :description, presence: true
  validates :year,
		numericality: {
			greater_than_or_equal_to: 2000,
			less_than_or_equal_to: 2100
		},
    uniqueness: {
      scope: :closure,
      message: 'A season closure for this year already exists',
    }
  validates :start_month,
		numericality: {
			allow_nil: true,
			greater_than_or_equal_to: 1,
			less_than_or_equal_to: 12
		}
  validates :start_day_of_month,
		numericality: {
			allow_nil: true,
			greater_than_or_equal_to: 1,
			less_than_or_equal_to: 31
		}
  validates :end_month,
		numericality: {
			allow_nil: true,
			greater_than_or_equal_to: 1,
			less_than_or_equal_to: 12
		}
  validates :end_day_of_month,
		numericality: {
			allow_nil: true,
			greater_than_or_equal_to: 1,
			less_than_or_equal_to: 31
		}

  def self.icon
    'lock'
  end

  def ransackable_attributes(auth_object = nil)
    %w[description]
  end

  def ransackable_associations(auth_object = nil)
    %w[]
  end

  after_save :touch_closure_cache
  after_destroy :touch_closure_cache
  def touch_closure_cache
    # trigger update of cached values
    closure.reset_calculations_cache
    closure.save!
  end

  has_paper_trail(
    meta: {
      geo_ref_id: proc { |sc| sc.closure.geo_ref_id }
    }
  )
end
