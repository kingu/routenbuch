class Region < GeoRef
  class << self
    def valid_parent_classes
      [Country, Region]
    end
  end
end
