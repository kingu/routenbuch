class Ticklist < ApplicationRecord
  validates :user, presence: true
  validates :route, presence: true, uniqueness: {
    scope: :user, message: "Route is already on your ticklist"
  }

  belongs_to :user
  belongs_to :route

  def self.icon
    'check-square-o'
  end

  def ransackable_attributes(auth_object = nil)
    %w[comment]
  end

  def ransackable_associations(auth_object = nil)
    %w[user route]
  end
end
