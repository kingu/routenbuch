class Version < ApplicationRecord
  include PaperTrail::VersionConcern

  # our routes should be associated with the tree in addtion
  # to the changed item which may be a sub-item of a route (inventory)
  # or a geo_ref (closure)
  belongs_to :geo_ref, optional: true
  validates :geo_ref, presence: true, unless: :standalone_version?

  belongs_to :route, optional: true

  # direct reference to the user object if available
  belongs_to :user, optional: true

  # object is not directly assigned to a node in the tree hierarchy
  def standalone_version?
    item.is_a? FirstAscentPerson
  end

  def self.icon
    'history'
  end
end
