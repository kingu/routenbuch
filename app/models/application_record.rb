class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # force whitelisting of ransack attributes, associations, ...
  def ransackable_attributes(auth_object = nil)
    []
  end

  def ransackable_associations(auth_object = nil)
    []
  end

  def ransortable_attributes(auth_object = nil)
    []
  end

  def ransackable_scopes(auth_object = nil)
    []
  end

  def as_base_class
    return self if self.class.base_class?

    return self.becomes(self.class.base_class)
  end

  def self.icon
    'question'
  end

  def icon
    self.class.icon
  end

  def display_name
    try(:name) || id.to_s
  end

  def self.display_name
    name
  end
end
