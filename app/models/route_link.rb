class RouteLink < ApplicationRecord
  class << self
    KIND_DESCRIPTIONS = {
      N_('belay') => N_('Shares a common belay with another route'),
      N_('start_with') => N_('Uses the start of another route'),
      N_('end_with') => N_('Uses the end of another route'),
      N_('overlap') => N_('Overlaps with another route'),
      N_('cross') => N_('Crosses another route'),
    }.freeze

    KINDS = KIND_DESCRIPTIONS.keys.freeze 

    def kind_descriptions
      KIND_DESCRIPTIONS
    end

    def kinds
      KINDS
    end

    def valid_kind?(kind)
      KINDS.include? kind
    end

    def icon
        'random'
    end
  end

  belongs_to :route_from, class_name: 'Route', foreign_key: 'route_from_id'
  belongs_to :route_to, class_name: 'Route', foreign_key: 'route_to_id'

  validates :kind, presence: true
  validate :validate_kind
  def validate_kind
    return if kind.blank?
    return if self.class.valid_kind? kind

    errors.add(:kind, _('is not a valid kind'))
  end

  validates :anchor_number, numericality: { only_integer: true, allow_nil: true }
  validates :first_anchor_number, numericality: { only_integer: true, allow_nil: true }
  validates :last_anchor_number, numericality: { only_integer: true, allow_nil: true }

  validate :validate_routes
  def validate_routes
    return unless route_to.present? && route_from.present?

    errors.add(:route_to, _('must be on the same crag')) unless route_from.geo_ref_id == route_to.geo_ref_id
  end

  def supports_first_anchor_number?
    %w[end_with cross overlap].include? kind
  end

  def supports_last_anchor_number?
    %w[start_with overlap].include? kind
  end

  validates(
    :first_anchor_number,
    absence: true,
    unless: :supports_first_anchor_number?
  )
  validates(
    :last_anchor_number,
    absence: true,
    unless: :supports_last_anchor_number?
  )

  def inventories
    query = \
    case kind
    when 'belay'
      route_to.inventories.where(role: 'belay')
    when 'cross'
      route_to.inventories.where(
        role: 'intermediate anchor',
        anchor_number: first_anchor_number
      ) if first_anchor_number.present?
    when 'start_with'
      route_to.inventories.where(
        'anchor_number <= ?', last_anchor_number
      ) if last_anchor_number.present?
    when 'end_with'
      route_to.inventories.where(
        'anchor_number >= ?', first_anchor_number
      ) if first_anchor_number.present?
    when 'overlap'
      route_to.inventories.where(
        'anchor_number >= ? AND anchor_number <= ?',
        first_anchor_number,
        last_anchor_number
      ) if first_anchor_number.present? && last_anchor_number.present?
    end

    return Inventory.none if query.nil?

    query
  end

  has_paper_trail(
    meta: {
      route_id: :route_from_id,
      geo_ref_id: proc { |l| l.route_from.geo_ref_id }
    }
  )
end
