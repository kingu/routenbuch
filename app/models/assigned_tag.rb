class AssignedTag < ApplicationRecord
  belongs_to :taggable, polymorphic: true
  belongs_to :tag

  def ransackable_attributes(auth_object = nil)
    %w[name description category]
  end

  def ransackable_associations(auth_object = nil)
    %w[]
  end
end
