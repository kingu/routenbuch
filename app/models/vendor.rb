class Vendor < ApplicationRecord
  has_many :products
  has_many :inventories, through: :products

  validates :name, presence: true
  validates :url, url: true

  def self.ransackable_attributes(_auth_object = nil)
    %w[name]
  end

  def self.ransackable_associations(_auth_object = nil)
    []
  end

  def self.icon
    'industry'
  end
end
