class Tag < ApplicationRecord
  has_many :assigned_tags
  has_many :taggables, through: :assigned_tags

  validates :name, :model, :description, presence: true
  validates(
    :name,
    uniqueness: {
      scope: :model,
      message: "tags can only exist once per model"
    }
  )

  def ransackable_attributes(auth_object = nil)
    %w[name description]
  end

  def ransackable_associations(auth_object = nil)
    %w[]
  end
end
