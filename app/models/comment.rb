class Comment < ApplicationRecord
  class << self
    def kinds
      [
        N_('comment'),
        N_('beta'),
        N_('maintenance_required'),
        N_('maintenance'),
        N_('nature'),
        N_('access'),
        N_('wrong_information'),
      ].freeze
    end

    def icon
      'comments'
    end
  end

  def icon
    'comment'
  end

  belongs_to :target, polymorphic: true
  belongs_to :user

  include TargetAccessFilterable

  validates(
    :kind,
    inclusion: {
      in: Comment.kinds,
      message: "%{value} is not a valid type for comment"
    },
    presence: true
  )

  validates :body, length: { maximum: 4096 }

  def ransackable_attributes(auth_object = nil)
    %w[kind private]
  end

  def ransackable_associations(auth_object = nil)
    %w[]
  end

  def user_modified?
    created_at != updated_at
  end
end
