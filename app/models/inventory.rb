class Inventory < ApplicationRecord
  class << self
    ROLES = [
      N_('belay'),
      N_('intermediate anchor')
    ].freeze

    def roles
      ROLES
    end

    def valid_role?(role)
      ROLES.include? role
    end

    def icon
      'database'
    end
  end

  belongs_to :route
  belongs_to :product
  delegate :vendor, to: :product

  validates(
    :anchor_number,
    presence: true,
    numericality: { only_integer: true }
  )

  validate :validate_role
  def validate_role
    return if role.blank?
    return if self.class.valid_role? role

    errors.add(:role, _('is not a valid role'))
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[anchor_number installed_at removed_at description]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[product vendor route]
  end

  has_paper_trail(
    meta: {
      route_id: :route_id,
      geo_ref_id: proc { |i| i.route.geo_ref_id }
    }
  )
end
