class ClosureReason < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  def ransackable_attributes(auth_object = nil)
    %w[name]
  end

  def ransackable_associations(auth_object = nil)
    %w[]
  end
end
