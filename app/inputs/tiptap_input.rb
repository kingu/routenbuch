class TiptapInput < SimpleForm::Inputs::Base
  def input(wrapper_options)
    name = options[:name]
    name ||= "#{@builder.object_name}[#{attribute_name}]"
    initial_value = @builder.object.send(attribute_name)

    template.content_tag(
      'tiptap-input',
      '',
      name: name,
      'initial-content': initial_value,
    ).html_safe
  end
end
