module Tiptap
  class Content
    attr_accessor :safe_content
    delegate :blank?, :empty?, :html_safe, :present?, to: :safe_content

    def initialize(content = nil)
      scrubber = Rails::Html::PermitScrubber.new
      scrubber.tags = %w[p ul li em strong br]
      scrubber.attributes = []

      fragment = Loofah.fragment(content)
      fragment.scrub!(scrubber)
      @safe_content = fragment.to_s
    end

    def to_s
      @safe_content.html_safe
    end
  end
end
