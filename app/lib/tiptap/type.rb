module Tiptap
  class Type < ActiveRecord::Type::Value
    def type
      :tiptap
    end

    # we expect a string or Tiptap::Content from all inputs (user, database)
    # always return a Tiptap::Content instance of it
    def cast_value(value)
      return if value.nil?
      return value if value.is_a? Tiptap::Content
      if value.is_a? ::String
        return if value.empty?
        return Tiptap::Content.new(value)
      end
      return
    end

    # the database needs a string
    def serialize(value)
      value.to_s
    end
  end
end
